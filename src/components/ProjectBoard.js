import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getBacklog } from "../actions/projectTaskActions";
import ProjectTaskItem from "./ProjectTask/ProjectTaskItem";

class ProjectBoard extends Component {
  cols = [
    {
      status: "TO_DO",
      text: "TO DO",
      background: "bg-secondary",
    },
    {
      status: "IN_PROGRESS",
      text: "In Progress",
      background: "bg-primary",
    },
    {
      status: "DONE",
      text: "Done",
      background: "bg-success",
    },
  ];

  componentDidMount() {
    this.props.getBacklog();
  }

  mapTaskToComp = (project_task) => (
    <ProjectTaskItem key={project_task.id} project_task={project_task} />
  );

  generateCols = () => this.cols.map(this.generateCol);

  generateCol = ({ status, text, background }) => (
    <div key={status} className="col-md-4">
      <div className="card text-center mb-2">
        <div className={`card-header ${background} text-white`}>
          <h3>{text}</h3>
        </div>
      </div>
      {this.props.project_tasks
        .filter((t) => t.status === status)
        .map(this.mapTaskToComp)}
    </div>
  );

  render() {
    const BoardContent =
      this.props.project_tasks.length < 1 ? (
        <div className="alert alert-info text-center" role="alert">
          No Project Tasks on this board
        </div>
      ) : (
        <React.Fragment>
          <div className="container">
            <div className="row">{this.generateCols()}</div>
          </div>
        </React.Fragment>
      );

    return (
      <div className="container">
        <Link to="/addProjectTask" className="btn btn-primary mb-3">
          <i className="fas fa-plus-circle"> Create Project Task</i>
        </Link>
        <br />
        <hr />
        {BoardContent}
      </div>
    );
  }
}

ProjectBoard.propTypes = {
  getBacklog: PropTypes.func.isRequired,
  project_tasks: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  project_tasks: state.project_task.project_tasks,
});

const mapDispatchToProps = { getBacklog };

export default connect(mapStateToProps, mapDispatchToProps)(ProjectBoard);
